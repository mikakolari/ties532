﻿using System;

namespace TIES532.Common
{
    public abstract class Change
    {
        public Guid ChangeId { get; set; }
        public Guid? BlockingChangeId { get; set; }
        public Change()
        {
            ChangeId = Guid.NewGuid();
        }
        public abstract ChangeType Type { get;  }
        public string Path { get; set; }
    }
    public class CreateDirectory : Change
    {
        public override ChangeType Type
        {
            get
            {
                return ChangeType.CreateDirectory;
            }
        }
        public string Name { get; set; }
        public string ParentDirectoryId { get; set; }
    }
    public class DeleteDirectory : Change
    {
        public override ChangeType Type
        {
            get
            {
                return ChangeType.DeleteDirectory;
            }
        }
        public string DirectoryId { get; set; }
    }
    public class CopyFile : Change
    {
        public override ChangeType Type
        {
            get
            {
                return ChangeType.CopyFile;
            }
        }
        public string Filename { get; set; }
        public string DirectoryId { get; set; }
        public string Url { get; set; }
        public int Size { get; set; }
        public string Key { get; set; }
    }
    public class MoveFile : Change
    {
        public override ChangeType Type
        {
            get
            {
                return ChangeType.MoveFile;
            }
        }
        public string FileId { get; set; }
        public string DirectoryId { get; set; }
    }
    public class DeleteFile : Change
    {
        public override ChangeType Type
        {
            get
            {
                return ChangeType.DeleteFile;
            }
        }
        public string FileId { get; set; }
    }

    public enum ChangeType
    {
        CreateDirectory,
        DeleteDirectory,
        CopyFile,
        MoveFile,
        DeleteFile
    }
}
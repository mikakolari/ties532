﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Common
{
    public interface IWebClient
    {
        T GetJson<T>(string url, object query);
        T GetJson<T>(string url, NameValueCollection query);
        T GetXml<T>(string url, object query);
        T GetXml<T>(string url, NameValueCollection query);
        Stream OpenRead(string url);
        WebRequest OpenRequest(string url);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Common
{
    public class Directory
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Directory> Directories { get; set; }
        public IEnumerable<File> Files { get; set; }
        public string Path
        {
            get
            {
                if(Parent == null)
                {
                    return "/";
                }
                else
                {
                    return Parent.Path + Name + "/";
                }
            }
        }
        public Directory Parent { get; set; }

        /// <summary>
        /// Get changes that are needed to target directory
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public IEnumerable<Change> GetChanges(Directory target)
        {
            return CheckChanges(this, target);
        }
        private IEnumerable<Change> CheckChanges(Directory source, Directory target, Guid? blockingChangeId = null)
        {
            var changes = new List<Change>();

            // Files to add
            if (source.Files != null)
            {
                foreach (var file in source.Files)
                {
                    if (target == null || target.Files == null || !target.Files.Any(f => f.Name == file.Name))
                    {
                        changes.Add(new CopyFile
                        {
                            Filename = file.Name,
                            DirectoryId = target != null ? target.Id : null,
                            BlockingChangeId = blockingChangeId,
                            Path = file.FullName,
                            Url = file.Url,
                            Size = file.Size,
                            Key = file.Key
                        });
                    }
                }
            }

            // Files to delete
            if (target != null && target.Files != null)
            {
                foreach (var file in target.Files)
                {
                    if (source.Files == null || !source.Files.Any(f => f.Name == file.Name))
                    {
                        changes.Add(new DeleteFile
                        {
                            FileId = file.Id,
                            Path = file.FullName
                        });
                    }
                }
            }

            // Directories to remove
            if (target != null && target.Directories != null)
            {
                foreach (var targetSubDirectory in target.Directories)
                {
                    Directory sourceSubDirectory = null;
                    if (source.Directories != null)
                    {
                        sourceSubDirectory = source.Directories.FirstOrDefault(d => d.Name == targetSubDirectory.Name);
                    }
                    if (sourceSubDirectory == null)
                    {
                        changes.Add(new DeleteDirectory
                        {
                            DirectoryId = targetSubDirectory.Id,
                            Path = targetSubDirectory.Path
                        });
                    }
                }
            }

            // Subdirectories
            if (source.Directories != null)
            {
                foreach (var sourceSubDirectory in source.Directories)
                {
                    Directory targetSubDirectory = null;
                    if (target != null && target.Directories != null)
                    {
                        targetSubDirectory = target.Directories.FirstOrDefault(d => d.Name == sourceSubDirectory.Name);
                    }
                    if (targetSubDirectory == null)
                    {
                        // Create directory
                        var change = new CreateDirectory
                        {
                            ParentDirectoryId = target != null ? target.Id : null,
                            Name = sourceSubDirectory.Name,
                            BlockingChangeId = blockingChangeId,
                            Path = sourceSubDirectory.Path
                        };
                        changes.Add(change);
                        changes.AddRange(CheckChanges(sourceSubDirectory, targetSubDirectory, change.ChangeId));
                    }
                    else
                    {
                        changes.AddRange(CheckChanges(sourceSubDirectory, targetSubDirectory, blockingChangeId));
                    }
                }
            }

            return changes;
        }
    }
}

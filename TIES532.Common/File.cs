﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Common
{
    public class File
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string FullName
        {
            get
            {
                return Directory.Path + Name;
            }
        }
        public string Url { get; set; }
        public string Key { get; set; }
        public Directory Directory { get; set; }
        
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace TIES532.Common
{
    public class WebClient : IWebClient
    {
        private const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36";
        public T GetJson<T>(string url, object query)
        {
            var queryString = QueryStringUtils.CreateQueryString(query);
            return GetJson<T>(url, queryString);
        }
        public T GetJson<T>(string url, NameValueCollection query)
        {
            using (var client = new System.Net.WebClient())
            {
                client.Headers.Add(HttpRequestHeader.UserAgent, UserAgent);
                client.QueryString = query;
                var response = client.DownloadString(url);
                return JsonConvert.DeserializeObject<T>(response);
            }
        }

        public T GetXml<T>(string url, object query)
        {
            var queryString = QueryStringUtils.CreateQueryString(query);
            return GetXml<T>(url, queryString);
        }

        public T GetXml<T>(string url, NameValueCollection query)
        {
            using (var client = new System.Net.WebClient())
            {
                client.Headers.Add(HttpRequestHeader.UserAgent, UserAgent);
                client.QueryString = query;
                var serializer = new XmlSerializer(typeof(T));
                using (var stream = client.OpenRead(url))
                {
                    return (T)serializer.Deserialize(stream);
                }
            }
        }
        public Stream OpenRead(string url)
        {
            using (var client = new System.Net.WebClient())
            {
                client.Headers.Add(HttpRequestHeader.UserAgent, UserAgent);
                return client.OpenRead(url);
            }
        }
        public WebRequest OpenRequest(string url)
        {
            var request = WebRequest.Create(url);
            if(request is HttpWebRequest)
            {
                (request as HttpWebRequest).UserAgent = UserAgent;
            }
            return request;
        }
    }
}

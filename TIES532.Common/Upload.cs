﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Common
{
    public abstract class Upload : IDisposable
    {
        public abstract void WriteData(byte[] buffer, int length);
        public abstract File Finish();
        public abstract void Dispose();
    }
}

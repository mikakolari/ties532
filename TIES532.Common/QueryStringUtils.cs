﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TIES532.Common
{
    public static class QueryStringUtils
    {
        public static NameValueCollection CreateQueryString(object query)
        {
            var querystring = new NameValueCollection();
            if (query != null)
            {
                foreach (var property in query.GetType().GetProperties())
                {
                    var value = property.GetValue(query);
                    if (value != null)
                    {
                        querystring.Add(property.Name, HttpUtility.UrlEncode(value.ToString()));
                    }
                }
            }
            return querystring;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    static class UrlHelper
    {
        private const string HTTP = "http://www.mediafire.com/api/1.4/";
        private const string HTTPS = "https://www.mediafire.com/api/1.4/";

        internal static string MakeHttp(string path)
        {
            if (path.StartsWith("/"))
            {
                return HTTP + path.Substring(1);
            }
            return HTTP + path;
        }
        internal static string MakeHttps(string path)
        {
            if (path.StartsWith("/"))
            {
                return HTTPS + path.Substring(1);
            }
            return HTTPS + path;
        }
    }
}

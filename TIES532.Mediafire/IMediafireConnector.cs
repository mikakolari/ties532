﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    public interface IMediafireConnector
    {
        string GetSessionToken(string email, string password, string appid, string apikey);
    }
}

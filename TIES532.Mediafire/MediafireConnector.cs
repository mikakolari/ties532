﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TIES532.Common;

namespace TIES532.Mediafire
{
    public class MediafireConnector : IMediafireConnector
    {
        private readonly IWebClient webClient;
        public MediafireConnector(IWebClient webClient)
        {
            this.webClient = webClient;
        }
        public string GetSessionToken(string email, string password, string appid, string apikey)
        {
            var url = UrlHelper.MakeHttps("user/get_session_token.php");
            var signature = GetSignature(email, password, appid, apikey);
            var query = new { email, password, application_id = appid, signature, response_format = "json" };
            var response = webClient.GetJson<ResponseWrapper<SessionTokenResponse>>(url, query).Response;
            return response.SessionToken;
        }
        private string GetSignature(string email, string password, string appid, string apikey)
        {
            byte[] buffer = Encoding.Default.GetBytes(email + password + appid + apikey);
            var sha1 = new SHA1CryptoServiceProvider();
            return BitConverter.ToString(sha1.ComputeHash(buffer)).Replace("-", "");
        }
    }
}

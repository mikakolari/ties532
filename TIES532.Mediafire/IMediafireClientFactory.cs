﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    public interface IMediafireClientFactory
    {
        IMediafireClient CreateClient(string email, string password);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TIES532.Common;

namespace TIES532.Mediafire
{
    public class MediafireClient : IMediafireClient
    {
        private readonly string appId;
        private readonly string apiKey;
        private readonly IWebClient webClient;
        private readonly string email;
        private readonly string password;
        private string sessionToken;

        public MediafireClient(string appId, string apiKey, string email, string password) :this(appId, apiKey, email, password, new WebClient())
        {
        }
        public MediafireClient(string appId, string apiKey, string email, string password, IWebClient webClient)
        {
            this.appId = appId;
            this.apiKey = apiKey;
            this.email = email;
            this.password = password;
            this.webClient = webClient;
        }

        public void Login()
        {
            this.sessionToken = GetSessionToken();
        }
        public Directory GetDirectory(string id = null)
        {
            /*
            var infoQuery = GetQueryStringWithDefaults(new { folder_key = id });
            var infoResponse = webClient.GetJson<ResponseWrapper<FolderInfoResponse>>(UrlHelper.MakeHttp("folder/get_info.php"), infoQuery).Response;
            var directory = new Directory
            {
                Id = infoResponse.FolderInfo.Key,
                Name = id != null ? infoResponse.FolderInfo.Name : null
            };*/
            var directory = new Directory
            {
                Id = id
            };
            var files = GetFiles(id);
            if (files != null)
            {
                directory.Files = files.Select(f => new File
                {
                    Id = f.QuickKey,
                    Name = f.Filename,
                    Size = f.Size,
                    Directory = directory,
                    Url = f.Links.DirectDownload,
                    Key = f.QuickKey
                }).OrderBy(f => f.Name).ToList();
            }
            
            var folders = GetSubFolders(id);
            if (folders != null)
            {
                var directories = new List<Directory>();
                foreach (var subFolder in folders)
                {
                    var subDirectory = GetDirectory(subFolder.Key);
                    subDirectory.Name = subFolder.Name;
                    subDirectory.Parent = directory;
                    directories.Add(subDirectory);
                }
                directory.Directories = directories.OrderBy(d => d.Name).ToList();
            }
            return directory;
        }
        public string GetDownloadUrl(string fileKey)
        {
            var query = GetQueryStringWithDefaults(new { quick_key = fileKey, link_type = "direct_download" });
            var respose = webClient.GetJson<ResponseWrapper<LinksResponse>>(UrlHelper.MakeHttp("file/get_links.php"), query).Response;
            return respose.Links[0].DirectDownload;
        }
        private IEnumerable<FolderInfo> GetSubFolders(string folderId, int chunk = 1)
        {
            var url = UrlHelper.MakeHttp("folder/get_content.php");
            var folders = new List<FolderInfo>();
            while (true)
            {
                var query = GetQueryStringWithDefaults(new { folder_key = folderId, content_type = "folders", chunk });
                var response = webClient.GetJson<ResponseWrapper<FolderContentResponse>>(url, query).Response;
                folders.AddRange(response.FolderContent.Folders);
                if (response.FolderContent.MoreChunks == YesNo.No)
                {
                    break;
                }
                chunk++;
            }
            return folders;
        }
        private IEnumerable<FileInfo> GetFiles(string folderId, int chunk = 1)
        {
            var url = UrlHelper.MakeHttp("folder/get_content.php");
            var files = new List<FileInfo>();
            while (true)
            {
                var query = GetQueryStringWithDefaults(new { folder_key = folderId, content_type = "files", chunk });
                var response = webClient.GetJson<ResponseWrapper<FolderContentResponse>>(url, query).Response;
                files.AddRange(response.FolderContent.Files);
                if (response.FolderContent.MoreChunks == YesNo.No)
                {
                    break;
                }
                chunk++;
            }
            return files;
        }

        private NameValueCollection GetQueryStringWithDefaults(object query = null)
        {
            var querystring = QueryStringUtils.CreateQueryString(query);
            querystring.Add("session_token", sessionToken);
            querystring.Add("response_format", "json");
            return querystring;
        }
        private string GetSignature(string email, string password, string appid, string apikey)
        {
            byte[] buffer = Encoding.Default.GetBytes(email + password + appid + apikey);
            var sha1 = new SHA1CryptoServiceProvider();
            return BitConverter.ToString(sha1.ComputeHash(buffer)).Replace("-", "");
        }
        private string GetSessionToken()
        {
            var url = UrlHelper.MakeHttps("user/get_session_token.php");
            var signature = GetSignature(email, password, appId, apiKey);
            var query = new { email, password, application_id = appId, signature, response_format = "json" };
            var response = webClient.GetJson<ResponseWrapper<SessionTokenResponse>>(url, query).Response;
            return response.SessionToken;
        }
    }
}

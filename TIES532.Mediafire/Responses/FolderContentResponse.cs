﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class FolderContentResponse : Response
    {
        [JsonProperty("folder_content")]
        public FolderContent FolderContent { get; set; }
    }
}

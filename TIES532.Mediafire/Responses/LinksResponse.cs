﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class LinksResponse : Response
    {
        [JsonProperty("links")]
        public Link[] Links { get; set; }
        [JsonProperty("direct_download_free_bandwidth")]
        public int FreeBandwidth { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class Link
    {
        [JsonProperty("quickkey")]
        public string QuickKey { get; set; }
        [JsonProperty("direct_download")]
        public string DirectDownload { get; set; }
    }
}

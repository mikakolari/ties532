﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class FileInfo
    {
        public string QuickKey { get; set; }
        public string Filename { get; set; }
        public int Size { get; set; }
        public Links Links { get; set; }
    }
}

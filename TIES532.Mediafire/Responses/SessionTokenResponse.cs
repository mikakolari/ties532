﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class SessionTokenResponse : Response
    {
        [JsonProperty("session_token")]
        public string SessionToken { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class Links
    {
        public string View { get; set; }
        public string Read { get; set; }
        [JsonProperty("normal_download")]
        public string NormalDownLoad { get; set; }
        [JsonProperty("direct_download")]
        public string DirectDownload { get; set; }
        [JsonProperty("one_time")]
        public OneTimeLinks OneTime { get; set; }

        public class OneTimeLinks
        {
            public string Download { get; set; }
            public string View { get; set; }
        }
    }
}

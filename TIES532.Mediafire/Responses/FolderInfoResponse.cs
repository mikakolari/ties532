﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class FolderInfoResponse : Response
    {
        [JsonProperty("folder_info")]
        public FolderInfo FolderInfo { get; set; }
    }
}

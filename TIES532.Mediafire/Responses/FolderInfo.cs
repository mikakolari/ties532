﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class FolderInfo
    {
        [JsonProperty("folderkey")]
        public string Key { get; set; }
        public string Name { get; set; }
        [JsonProperty("created_utc")]
        public DateTime CreatedUtc { get; set; }
        [JsonProperty("file_count")]
        public int FileCount { get; set; }
        [JsonProperty("folder_count")]
        public int FolderCount { get; set; }
        [JsonProperty("total_files")]
        public int TotalFiles { get; set; }
        [JsonProperty("total_folders")]
        public int TotalFolders { get; set; }
        
    }
}

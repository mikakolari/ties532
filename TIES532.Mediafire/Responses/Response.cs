﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    abstract class Response
    {
        public string Action { get; set; }
        public Result Result { get; set; }
        [JsonProperty("new_key")]
        public YesNo NewKey { get; set; }
    }
}

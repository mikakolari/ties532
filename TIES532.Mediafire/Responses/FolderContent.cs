﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    class FolderContent
    {
        [JsonProperty("chunk_size")]
        public int ChunkSize { get; set; }
        [JsonProperty("chunk_number")]
        public int ChunkNumber { get; set; }
        [JsonProperty("more_chunks")]
        public YesNo MoreChunks { get; set; }
        [JsonProperty("content_type")]
        public ContentType ContentType { get; set; }
        public FolderInfo[] Folders { get; set; }
        public FileInfo[] Files { get; set; }
    }
  
}

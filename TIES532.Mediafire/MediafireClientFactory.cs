﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Mediafire
{
    public class MediafireClientFactory : IMediafireClientFactory
    {
        private readonly string appId;
        private readonly string apiKey;
        public MediafireClientFactory(string appId, string apiKey)
        {
            this.appId = appId;
            this.apiKey = apiKey;
        }
        public IMediafireClient CreateClient(string email, string password)
        {
            return new MediafireClient(appId, apiKey, email, password);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Common;

namespace TIES532.Mediafire
{
    public interface IMediafireClient
    {
        void Login();
        Directory GetDirectory(string id = null);
        string GetDownloadUrl(string fileKey);
    }
}

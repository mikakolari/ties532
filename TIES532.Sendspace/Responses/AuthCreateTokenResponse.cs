﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    [XmlRoot(ElementName = "result")]
    public class AuthCreateTokenResponse : Response
    {
        [XmlElement("token")]
        public string Token { get; set; }
    }
}

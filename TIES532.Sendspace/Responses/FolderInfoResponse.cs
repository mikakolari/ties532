﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    [XmlRoot(ElementName = "result")]
    public class FolderInfoResponse : Response
    {
        [XmlElement("folder")]
        public FolderInfo Folder { get; set; }
    }
}

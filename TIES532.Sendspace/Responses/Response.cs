﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    [XmlRoot(ElementName = "result")]
    public class Response
    {
        [XmlAttribute("method")]
        public string Method { get; set; }
        [XmlAttribute("status")]
        public string Status { get; set; }
        [XmlElement("error")]
        public Error Error { get; set; }

        public bool Ok
        {
            get
            {
                return Status == "ok";
            }
        }
    }
}

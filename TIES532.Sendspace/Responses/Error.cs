﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    public class Error
    {
        [XmlAttribute("code")]
        public int Code { get; set; }
        [XmlAttribute("text")]
        public string Text { get; set; }
        [XmlAttribute("info")]
        public string Info { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    [XmlRoot(ElementName = "result")]
    public class UploadInfoResponse : Response
    {
        [XmlElement("upload")]
        public UploadInfo Upload { get; set; }
    }
}

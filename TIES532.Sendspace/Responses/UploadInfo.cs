﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    public class UploadInfo
    {
        [XmlAttribute("upload_identifier")]
        public string Id { get; set; }
        [XmlAttribute("url")]
        public string Url { get; set; }
        [XmlAttribute("progress_url")]
        public string ProgressUrl { get; set; }
        [XmlAttribute("max_file_size")]
        public int MaxFileSize { get; set; }
        [XmlAttribute("extra_info")]
        public string ExtraInfo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TIES532.Sendspace
{
    public class FolderInfo
    {
        [XmlAttribute("id")]
        public string Id { get; set; }
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("parent_folder_id")]
        public string ParentId { get; set; }
        /*
        [XmlAttribute("total_folders")]
        public int? TotalFolders { get; set; }
        [XmlAttribute("total_files")]
        public int? TotalFiles { get; set; }
        */
    }
}

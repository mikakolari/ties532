﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Common;

namespace TIES532.Sendspace
{
    public interface ISendspaceClient
    {
        void Login();
        /// <summary>
        /// Get directory info recursively
        /// </summary>
        /// <param name="id">Directory id</param>
        /// <returns></returns>
        Directory GetDirectory(string id = "0");
        /// <summary>
        /// Create directory
        /// </summary>
        /// <param name="name">Directory name</param>
        /// <param name="parentId">Parent directory id</param>
        /// <returns></returns>
        Directory CreateDirectory(string name, string parentId);
        /// <summary>
        /// Delete directory
        /// </summary>
        /// <param name="id">Directory id</param>
        void DeleteDirectory(string id);
        TIES532.Common.Upload StartUpload(string filename, string directoryId);

        void DeleteFile(string id);
    }
}

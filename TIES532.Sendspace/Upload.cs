﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TIES532.Common;

namespace TIES532.Sendspace
{
    class Upload : Common.Upload
    {
        private readonly WebRequest request;
        private readonly string boundary;
        private readonly ISendspaceClient client;
        private readonly Stream requestStream;
        public Upload(WebRequest request, string boundary, ISendspaceClient client)
        {
            this.request = request;
            this.boundary = boundary;
            this.client = client;
            this.requestStream = request.GetRequestStream();
        }
        public override void WriteData(byte[] buffer, int length)
        {
            requestStream.Write(buffer, 0, length);
            requestStream.Flush();
        }
        public override Common.File Finish()
        {
            var writer = new StreamWriter(requestStream);
            writer.WriteLine();
            writer.WriteLine("--" + boundary + "--");
            writer.Flush();
            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var body = reader.ReadToEnd();
                var fileId = GetFileId(body);
                return null;
            }
        }
        public override void Dispose()
        {
            requestStream.Dispose();
        }
        private string GetFileId(string response)
        {
            return null;
        }
    }
}

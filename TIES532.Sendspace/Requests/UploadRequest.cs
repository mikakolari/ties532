﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Sendspace
{
    public class UploadRequest : UploadInfo
    {
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public string FolderId { get; set; }
    }
}

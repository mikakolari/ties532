﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Sendspace
{
    public class SendspaceClientFactory : ISendspaceClientFactory
    {
        private readonly string apiKey;
        public SendspaceClientFactory(string apiKey)
        {
            this.apiKey = apiKey;
        }
        public ISendspaceClient CreateClient(string username, string password)
        {
            return new SendspaceClient(apiKey, username, password);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Common;
using Directory = TIES532.Common.Directory;
using File = TIES532.Common.File;

namespace TIES532.Sendspace
{
    public class SendspaceClient : ISendspaceClient
    {
        private readonly string apiKey;
        private readonly string username;
        private readonly string password;
        private string sessionToken;
        private readonly IWebClient webClient;

        public SendspaceClient(string apiKey, string username, string password) : this(apiKey, username, password, new WebClient())
        {
        }
        public SendspaceClient(string apiKey, string username, string password, IWebClient webClient)
        {
            this.apiKey = apiKey;
            this.username = username;
            this.password = password;
            this.webClient = webClient;  
        }

        public void Login()
        {
            this.sessionToken = GetSessionToken();
        }
        /// <summary>
        /// Get directory info recursively
        /// </summary>
        /// <param name="id">Directory id</param>
        /// <returns></returns>
        public Directory GetDirectory(string id = "0")
        {
            var infoQuery = GetQueryStringWithDefaults(new { method = "folders.getInfo", folder_id = id });
            var infoResponse = webClient.GetXml<FolderInfoResponse>(UrlHelper.HTTP, infoQuery);
            if (!infoResponse.Ok)
            {

            }
            var contentQuery = GetQueryStringWithDefaults(new { method = "folders.getContents", folder_id = id });
            var contentResponse = webClient.GetXml<FolderContentResponse>(UrlHelper.HTTP, contentQuery);

            var directory = new Directory
            {
                Id = infoResponse.Folder.Id,
                Name = infoResponse.Folder.Name
            };
            if (contentResponse.Files != null)
            {
                directory.Files = contentResponse.Files.Select(f => new File
                {
                    Id = f.Id,
                    Name = f.Name,
                    Size = f.Size,
                    Directory = directory
                }).ToList();
            }
            if (contentResponse.Folders != null)
            {
                var subDirectories = new List<Directory>();
                foreach (var subFolder in contentResponse.Folders)
                {
                    var subDirectory = GetDirectory(subFolder.Id);
                    subDirectory.Parent = directory;
                    subDirectories.Add(subDirectory);
                }
                directory.Directories = subDirectories;
            }
            return directory;
        }
        /// <summary>
        /// Create directory
        /// </summary>
        /// <param name="name">Directory name</param>
        /// <param name="parentId">Parent directory id</param>
        /// <returns></returns>
        public Directory CreateDirectory(string name, string parentId)
        {
            var query = GetQueryStringWithDefaults(new { method = "folders.create", name, parent_folder_id = parentId });
            var response = webClient.GetXml<FolderCreateResponse>(UrlHelper.HTTP, query);
            return new Directory
            {
                Id = response.Folder.Id,
                Name = response.Folder.Name
            };
        }
        /// <summary>
        /// Delete directory
        /// </summary>
        /// <param name="id">Directory id</param>
        public void DeleteDirectory(string id)
        {
            throw new NotImplementedException();
        }

       

        public TIES532.Common.Upload StartUpload(string filename, string directoryId)
        {
            var query = GetQueryStringWithDefaults(new { method = "upload.getInfo" });
            var uploadResponse = webClient.GetXml<UploadInfoResponse>(UrlHelper.HTTP, query);
            var fields = new
            {
                MAX_FILE_SIZE = uploadResponse.Upload.MaxFileSize,
                UPLOAD_IDENTIFIER = uploadResponse.Upload.Id,
                extra_info = uploadResponse.Upload.ExtraInfo,
                folder_id = directoryId
            };

            
            var boundary = "boundary" + Guid.NewGuid();
            var request = webClient.OpenRequest(uploadResponse.Upload.Url);
            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            var writer = new StreamWriter(request.GetRequestStream());
            
            WriteFields(writer, fields, boundary);

            writer.WriteLine("--" + boundary);
            writer.WriteLine("Content-Disposition: form-data; name=\"userfile\"; filename=\"" + filename + "\"");
            writer.WriteLine("Content-Type: application/octet-stream");
            writer.WriteLine();
            writer.Flush();
            return new Upload(request, boundary, this);
            
        }

        public void DeleteFile(string id)
        {
            throw new NotImplementedException();
        }

        private NameValueCollection GetQueryStringWithDefaults(object query = null)
        {
            var querystring = QueryStringUtils.CreateQueryString(query);
            querystring.Add("session_key", sessionToken);
            return querystring;
        }
        private string MD5(string input)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var inputBytes = Encoding.UTF8.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
        private string GetSessionToken()
        {
            var tokenQuery = new { method = "auth.createtoken", api_key = apiKey, api_version = "1.0" };
            try
            {
                var tokenResponse = webClient.GetXml<AuthCreateTokenResponse>(UrlHelper.HTTP, tokenQuery);
                var token = tokenResponse.Token;
                var tokenedPassword = MD5(token + MD5(password));
                var sessionQuery = new { method = "auth.login", api_key = apiKey, api_version = "1.0", token = token, user_name = username, tokened_password = tokenedPassword };
                var sessionResponse = webClient.GetXml<AuthLoginResponse>(UrlHelper.HTTP, sessionQuery);
                if (!sessionResponse.Ok)
                {
                    throw new UnauthorizedException();
                }
                return sessionResponse.SessionToken;
            }
            catch
            {
                throw;
            }
        }
        private void WriteFields(StreamWriter writer, object fields, string boundary)
        {
            foreach(var property in fields.GetType().GetProperties())
            {
                var value = property.GetValue(fields);

                writer.WriteLine("--" + boundary);
                writer.WriteLine("Content-Disposition: form-data; name=\"" + property.Name + "\"");
                writer.WriteLine();
                writer.WriteLine(value);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;

namespace TIES532.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Session_End()
        {
            var token = HttpContext.Current.Session[Config.ClientTokenSessionKey] as Guid?;
            if (token.HasValue)
            {
                var clientManager = OwinStartup.Kernel.Get<IClientManager>();
                if (clientManager != null)
                {
                    clientManager.Clear(token.Value);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.SessionState;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web
{
    class SessionUserData : IUserData
    {
        private readonly HttpSessionState session;

        public SessionUserData(HttpSessionState session)
        {
            this.session = session;
        }

        public IMediafireClient MediafireClient
        {
            get
            {
                return session[Config.MediafireClientSessionKey] as IMediafireClient;
            }
            set
            {
                session[Config.MediafireClientSessionKey] = value;
            }
        }

        public ISendspaceClient SendspaceClient
        {
            get
            {
                return session[Config.SendspaceClientSessionKey] as ISendspaceClient;
            }
            set
            {
                session[Config.SendspaceClientSessionKey] = value;
            }
        }
    }
}

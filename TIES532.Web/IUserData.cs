﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web
{
    public interface IUserData
    {
        IMediafireClient MediafireClient { get; set; }
        ISendspaceClient SendspaceClient { get; set; }
    }
}

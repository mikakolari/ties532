﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Web
{
    public enum FileStatus
    {
        Ok,
        Added,
        Changed,
        Deleted
    }
}

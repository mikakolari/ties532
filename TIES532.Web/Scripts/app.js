﻿$(document).ready(function () {

    $.connection.hub.start().done(function () {
        var controller = $.connection.syncController;
        var viewModel = new SyncViewModel(controller.server);
        ko.applyBindings(viewModel);
    });
});
function SyncViewModel(server) {
    var self = this;

    // views
    this.displayLogin = ko.observable(true);
    this.displayLoading = ko.observable(false);
    this.displayChanges = ko.observable(false);
    this.started = ko.observable(false);

    // mediafire
    this.mediafireEmail = ko.observable('');
    this.mediafirePassword = ko.observable('');
    this.mediafireError = ko.observable();
    
    // sendspace
    this.sendspaceUsername = ko.observable('');
    this.sendspacePassword = ko.observable('');
    this.sendspaceError = ko.observable();

    this.clientToken = undefined;

    // login
    this.login = function () {
        self.mediafireError();
        self.sendspaceError();
        self.displayLogin(false);
        self.displayLoading(true);

        $.post('/api/connect/', {
            mediafireEmail: self.mediafireEmail(),
            mediafirePassword: self.mediafirePassword(),
            sendspaceUsername: self.sendspaceUsername(),
            sendspacePassword: self.sendspacePassword()
        }).done(function (response) {
            self.clientToken = response.token;
            self.loadChanges();
        }).fail(function (response) {
            self.mediafireError(response.responseJSON.mediafireError);
            self.sendspaceError(response.responseJSON.sendspaceError);
            self.displayLoading(false);
            self.displayLogin(true);
        });
    };

    this.logout = function () {
        $.post('/api/logout/').always(function () {
            self.started(false);
            self.displayLogin(true);
            self.displayChanges(false);
        });
    };
    // changes
    this.changes = ko.observableArray();
    this.changesError = ko.observable();
    this.loadChanges = function () {
        self.changes.removeAll();
        self.changesError('');
        $.get('/api/changes/', { token: self.clientToken }).done(function (response) {
            for (var i in response) {
                var change = response[i];
                var model = new ChangeViewModel(change);
                self.changes.push(model);
            }
            self.displayLoading(false);
            self.displayChanges(true);
        }).fail(function () {
            self.changesError('Loading changes failed');
        });
    };
    this.start = function () {
        self.started(true);
        var changes = self.changes();
        for (var i in changes) {
            if (!changes[i].data.blockingChangeId) {
                process(changes[i]);
            }
        }
    };
    function process(change) {
        change.error(false);
        change.started(true);
        if (!self.started()) {
            return;
        }
        if (change.done()) {
            processDependants(change.data.changeId, result.id);
            return;
        }
        switch (change.data.type) {
            case 'CreateDirectory':
                console.log('CreateDirectory:Start', change.data);
                server.createDirectory(self.clientToken, change.data).done(function (result) {
                    console.log('CreateDirectory:Done', change.data);
                    change.progress(100);
                    change.done(true);
                    processDependants(change.data.changeId, result.id);
                }).fail(function () {
                    change.error(true);
                });
                break;
            case 'CopyFile':
                console.log('CopyFile:Start', change.data);
                server.copyFile(self.clientToken, change.data).progress(function (progress) {
                    change.progress(progress);
                }).done(function () {
                    console.log('CopyFile:Done', change.data);
                    change.progress(100);
                    change.done(true);
                }).fail(function () {
                    change.error(true);
                });
                break;
        }
    };
    function processDependants(changeId, directoryId) {
        var changes = self.changes();
        for (var i in changes) {
            if (changes[i].data.blockingChangeId == changeId) {
                switch (changes[i].data.type) {
                    case 'CreateDirectory':
                        changes[i].data.parentDirectoryId = directoryId;
                        break;
                    case 'CopyFile':
                        changes[i].data.directoryId = directoryId;
                        break;
                }
                process(changes[i]);
            }
        }
    };
}
function ChangeViewModel(change) {
    this.data = change;
    this.started = ko.observable(false);
    this.done = ko.observable(false);
    this.progress = ko.observable(0);
    this.error = ko.observable(false);
}
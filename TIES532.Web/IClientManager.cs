﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web
{
    public interface IClientManager
    {
        Guid Store(IMediafireClient mediafireClient, ISendspaceClient sendspaceClient);
        void Clear(Guid token);
        IMediafireClient GetMediafireClient(Guid token);
        ISendspaceClient GetSendspaceClient(Guid token);
    }
}

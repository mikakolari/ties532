﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Ninject;
using Owin;
using System.Configuration;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using TIES532.Common;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web
{
    public class OwinStartup
    {
        public static IKernel Kernel { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            var kernel = new StandardKernel();
            kernel.Bind<HttpSessionState>().ToMethod(c => HttpContext.Current.Session);
            //kernel.Bind<IUserData>().To<SessionUserData>();
            kernel.Bind<IWebClient>().To<WebClient>();
            kernel.Bind<IClientManager>().To<InMemoryClientManager>().InSingletonScope();

            var mediafireAppid = ConfigurationManager.AppSettings["Mediafire:appid"];
            var mediafireApikey = ConfigurationManager.AppSettings["Mediafire:apikey"];
            var mediafireClientFactory = new MediafireClientFactory(mediafireAppid, mediafireApikey);
            kernel.Bind<IMediafireClientFactory>().ToConstant(mediafireClientFactory);
            //kernel.Bind<IMediafireClient>().ToMethod(c =>  c.Kernel.Get<IUserData>().MediafireClient);

            var sendspaceApikey = ConfigurationManager.AppSettings["Sendspace:apikey"];
            var sendspaceClientFactory = new SendspaceClientFactory(sendspaceApikey);
            kernel.Bind<ISendspaceClientFactory>().ToConstant(sendspaceClientFactory);
            //kernel.Bind<ISendspaceClient>().ToMethod(c => c.Kernel.Get<IUserData>().SendspaceClient);

            // SignalR
            var resolver = new NinjectSignalRDependencyResolver(kernel);
            var hubconfig = new HubConfiguration();
            hubconfig.Resolver = resolver;
            app.MapSignalR(hubconfig);

            // Web Api
            var httpConfig = new HttpConfiguration();
            httpConfig.Routes.MapHttpRoute("connect", "api/connect", new { controller = "Connect", action = "Connect" });
            httpConfig.Routes.MapHttpRoute("logout", "api/logout", new { controller = "Connect", action = "Logout" });
            httpConfig.Routes.MapHttpRoute("api", "api/{controller}/{action}", new { action = "List" });
            httpConfig.DependencyResolver = new NinjectWebApiDependencyResolver(kernel);
            httpConfig.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            httpConfig.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            app.UseWebApi(httpConfig);
        }
    }
}
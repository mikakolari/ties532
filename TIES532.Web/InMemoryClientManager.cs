﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web
{
    class InMemoryClientManager : IClientManager
    {
        private static ConcurrentDictionary<Guid, IMediafireClient> mediafireClients = new ConcurrentDictionary<Guid, IMediafireClient>();
        private static ConcurrentDictionary<Guid, ISendspaceClient> sendspaceClients = new ConcurrentDictionary<Guid, ISendspaceClient>();
        public Guid Store(IMediafireClient mediafireClient, ISendspaceClient sendspaceClient)
        {
            var guid = Guid.NewGuid();
            if(!mediafireClients.TryAdd(guid, mediafireClient) || !sendspaceClients.TryAdd(guid, sendspaceClient))
            {
                throw new ApplicationException();
            }
            return guid;
        }
        public void Clear(Guid token)
        {
            IMediafireClient mediafireClient;
            mediafireClients.TryRemove(token, out mediafireClient);
            ISendspaceClient sendspaceClient;
            sendspaceClients.TryRemove(token, out sendspaceClient);
        }
        public IMediafireClient GetMediafireClient(Guid token)
        {
            return mediafireClients[token];
        }

        public ISendspaceClient GetSendspaceClient(Guid token)
        {
            return sendspaceClients[token];
        }
    }
}

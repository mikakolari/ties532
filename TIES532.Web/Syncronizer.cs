﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIES532.Common;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web
{
    class Syncronizer
    {
        private readonly IMediafireClient mediafireClient;
        private readonly ISendspaceClient sendspaceClient;
        public Syncronizer(string mediafireSessionToken, string sendspaceSessionToken) : this(new MediafireClient(mediafireSessionToken), new SendspaceClient(sendspaceSessionToken))
        {
        }
        public Syncronizer(MediafireClient mediafireClient, SendspaceClient sendspaceClient)
        {
            this.mediafireClient = mediafireClient;
            this.sendspaceClient = sendspaceClient;
        }

        public IEnumerable<FileDetails> GetChanges()
        {
            var mediafireDirectory = mediafireClient.GetDirectory();
            var sendspaceDirectory = sendspaceClient.GetDirectory();
            return null;
        }
        private IEnumerable<FileDetails> ProcessFiles(Directory mediafireDirectory, string pathPrefix = "")
        {
            var random = new Random();
            var files = new List<FileDetails>();
            var directoryPath = pathPrefix + mediafireDirectory.Name + "/";

            files.AddRange(mediafireDirectory.Files.Select(f => new FileDetails
            {
                Id = f.Id,
                Directory = directoryPath,
                FileName = f.Name,
                Size = f.Size + " b",
                Status = (FileStatus)random.Next(0, 4)
            }));
            foreach (var subDirectory in mediafireDirectory.Directories)
            {
                files.AddRange(ProcessFiles(subDirectory, directoryPath));
            }
            return files;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Web
{
    public class FileDetails
    {
        public string Id { get; set; }
        public string Directory { get; set; }
        public string FileName { get; set; }
        public string Size { get; set; }
        public FileStatus Status { get; set; }
    }
}

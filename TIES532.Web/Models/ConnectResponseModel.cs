﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Web.Models
{
    public class ConnectResponseModel
    {
        public string MediafireError { get; set; }
        public string SendspaceError { get; set; }
        public Guid Token { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Web.Models
{

    public class ConnectRequestModel
    {
        public string MediafireEmail { get; set; }
        public string MediafirePassword { get; set; }
        public string SendspaceUsername { get; set; }
        public string SendspacePassword { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIES532.Web.Models
{
    public class CreateDirectoryResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
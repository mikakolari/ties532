﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIES532.Web.Models
{
    public class ChangesResponseModel
    {
        public IEnumerable<FileDetails> Files { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using TIES532.Common;
using TIES532.Mediafire;
using TIES532.Sendspace;
using TIES532.Web.Models;

namespace TIES532.Web.Controllers
{
    public class ConnectController : ApiController
    {
        private readonly IMediafireClientFactory mediafireClientFactory;
        private readonly ISendspaceClientFactory sendspaceClientFactory;
        private readonly IClientManager clientManager;
        private readonly HttpSessionState session;

        public ConnectController(IMediafireClientFactory mediafireClientFactory, ISendspaceClientFactory sendspaceClientFactory, IClientManager clientManager, HttpSessionState session)
        {
            this.mediafireClientFactory = mediafireClientFactory;
            this.sendspaceClientFactory = sendspaceClientFactory;
            this.clientManager = clientManager;
            this.session = session;
        }
        [HttpPost]
        public HttpResponseMessage Connect(ConnectRequestModel model)
        {
            var mediafireClient = mediafireClientFactory.CreateClient(model.MediafireEmail, model.MediafirePassword);
            var sendspaceClient = sendspaceClientFactory.CreateClient(model.SendspaceUsername, model.SendspacePassword);

            var mediafireTask = Task.Run(() => mediafireClient.Login());
            var sendspaceTask = Task.Run(() => sendspaceClient.Login());
            try
            {
                Task.WaitAll(mediafireTask, sendspaceTask);
            }
            catch { }
            var response = new ConnectResponseModel();

            if(mediafireTask.IsFaulted)
            {
                response.MediafireError = "Login failed";
            }
            if (sendspaceTask.IsFaulted)
            {
                response.SendspaceError = "Login failed";
            }

            if(!string.IsNullOrWhiteSpace(response.MediafireError) || !string.IsNullOrWhiteSpace(response.SendspaceError))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
            }

            var token = clientManager.Store(mediafireClient, sendspaceClient);
            session[Config.ClientTokenSessionKey] = token;
            response.Token = token;

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        [HttpPost]
        public IHttpActionResult Logout()
        {
            var token = session[Config.ClientTokenSessionKey] as Guid?;
            if (token.HasValue)
            {
                clientManager.Clear(token.Value);  
            }
            return Ok();
        }
    }
}
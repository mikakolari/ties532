﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TIES532.Common;
using TIES532.Mediafire;
using TIES532.Sendspace;

namespace TIES532.Web.Controllers
{
    public class ChangesController : ApiController
    {
        //private readonly IMediafireClient mediafireClient;
        //private readonly ISendspaceClient sendspaceClient;
        private readonly IClientManager clientManager;

        public ChangesController(IClientManager clientManager)
        {
            //this.mediafireClient = mediafireClient;
            //this.sendspaceClient = sendspaceClient;
            this.clientManager = clientManager;
        }

        [HttpGet]
        public IHttpActionResult List(Guid token)
        {
            var mediafireClient = clientManager.GetMediafireClient(token);
            var sendspaceClient = clientManager.GetSendspaceClient(token);

            var mediafireTask = Task.Run(() => mediafireClient.GetDirectory());
            var sendspaceTask = Task.Run(() => sendspaceClient.GetDirectory());
            Task.WaitAll(mediafireTask, sendspaceTask);
            
            var mediafireDirectory = mediafireTask.Result;
            var sendspaceDirectory = sendspaceTask.Result;
            var changes = mediafireDirectory.GetChanges(sendspaceDirectory);
            return Ok(changes);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using TIES532.Mediafire;
using TIES532.Sendspace;
using Microsoft.AspNet.SignalR.Hubs;
using TIES532.Common;
using TIES532.Web.Models;

namespace TIES532.Web
{
    public class SyncController : Hub
    {
        private readonly IClientManager clientManager;
        private readonly IWebClient webClient;

        public SyncController(IClientManager clientManager, IWebClient webClient)
        {
            this.webClient = webClient;
            this.clientManager = clientManager;
        }
        public CreateDirectoryResponse CreateDirectory(Guid token, CreateDirectory change)
        {
            var sendspaceClient = GetSendspaceClient(token);
            System.Threading.Thread.Sleep(1000);
            var directory = sendspaceClient.CreateDirectory(change.Name, change.ParentDirectoryId);
            return new CreateDirectoryResponse { Id = directory.Id };
        }
        public void DeleteDirectory(Guid token, DeleteDirectory change)
        {
            var sendspaceClient = GetSendspaceClient(token);
            //sendspaceClient.DeleteDirectory(change.DirectoryId);
        }

        public void CopyFile(Guid token, CopyFile change, IProgress<int> progress)
        {
            var mediafireClient = GetMediafireClient(token);
            var downloadUrl = mediafireClient.GetDownloadUrl(change.Key);
            // get url - 20%
            progress.Report(20);
            var sendspaceClient = GetSendspaceClient(token);
            using (var upload = sendspaceClient.StartUpload(change.Filename, change.DirectoryId))
            using (var inStream = webClient.OpenRead(downloadUrl))
            {
                // get upload info - 20%
                progress.Report(40);
                var bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                int bytesWritten = 0;
                while (true)
                {
                    var readLength = inStream.Read(buffer, 0, bufferSize);
                    if (readLength == 0)
                    {
                        break;
                    }
                    upload.WriteData(buffer, readLength);
                    bytesWritten += readLength;
                    // copy data - rest 60%
                    progress.Report(40 + (int)((bytesWritten / (decimal)change.Size) * 60));
                }
                upload.Finish();
            }
        }

        public void DeleteFile(Guid token, DeleteFile change)
        {
            var sendspaceClient = GetSendspaceClient(token);
            //sendspaceClient.DeleteFile(change.FileId);
        }
        private IMediafireClient GetMediafireClient(Guid token)
        {
            return clientManager.GetMediafireClient(token);
        }
        private ISendspaceClient GetSendspaceClient(Guid token)
        {
            return clientManager.GetSendspaceClient(token);
        }
    }
}